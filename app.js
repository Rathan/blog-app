var express = require("express"),
	methodOverride = require("method-override"),
	expressSanitizer= require("express-sanitizer"),
	app = express(), 
	bodyParser = require("body-parser"),
	mongoose = require("mongoose");

//APP CONFIG
mongoose.connect("mongodb://localhost/restful_blogapp",{useNewUrlParser: true, useUnifiedTopology: true});
// Make Mongoose use `findOneAndUpdate()`. Note that this option is `true`
// by default, you need to set it to false.
mongoose.set('useFindAndModify', false);

// look for file with .ejs convention
app.set("view engine", "ejs");

// say express to look for public directory for custom style sheet
app.use(express.static("public"));
// tell express to use bodyparser
app.use(bodyParser.urlencoded({extended : true}));
app.use(expressSanitizer());
app.use(methodOverride("_method"));


//MONGOOSE MODEL CONFIG
// DB schema..schema with a capital S
var blogSchema = new mongoose.Schema({
	title: String,
	image: String,
	body: String,
	created: {type: Date, default: Date.now},
});

//compile it into a model
var Blog = mongoose.model("Blog", blogSchema);


//-------ROUTES------------
//GET route
app.get("/", function(req, res){
	res.redirect("/blogs");
});

app.get("/blogs", function(req, res){
	Blog.find({}, function(err, blogs){
		if(err){
			console.log(err);
		} else {
			res.render("index",{blogs:blogs});
		}
	});
	
});
//NEW ROUTE
app.get("/blogs/new", function(req, res){
	res.render("new");
});
//CREATE ROUTE
app.post("/blogs", function(req, res){
	//create new blog
	req.body.blog.body = req.sanitize(req.body.blog.body)
	Blog.create(req.body.blog, function(err, newBlog){
		if(err){
			res.render("new")
		} else {
			//redirect to index page
			res.redirect("/blogs")
		}
	});
	
	
});

//SHOW ROUTE

app.get("/blogs/:id", function(req, res){
	Blog.findById(req.params.id, function(err, foundBlog){
		if(err){
			res.redirect("/blogs");
		} else {
			res.render("show", {blog: foundBlog});
		}
	});
});

//EDIT ROUTE

app.get("/blogs/:id/edit", function(req, res){
	Blog.findById(req.params.id, function(err, foundBlog){
		if(err){
			res.redirect("/blogs");
		} else {
			res.render("edit", {blog: foundBlog});
		}
	});
	
});

//UPDATE ROUTE

app.put("/blogs/:id", function(req, res){
	// sanitize disables the user to run any javascript code in input area
	req.body.blog.body = req.sanitize(req.body.blog.body)
	Blog.findByIdAndUpdate(req.params.id, req.body.blog, function(err, updatedBlog){
		if(err){
			res.redirect("/blogs");
		}else {
			res.redirect("/blogs/"+ req.params.id);
		}
	});
});

//DELETE ROUTE
app.delete("/blogs/:id", function(req, res){
	// remove the blog
	Blog.findByIdAndRemove(req.params.id, function(err){
		if(err){
			res.redirect("/blogs");
		} else{
			res.redirect("/blogs");
		}
	});
});


app.listen(process.env.PORT=3000, process.env.IP, function(){
	console.log(" the blog app server has started");
});